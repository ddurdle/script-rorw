import sys
import os
import xbmc
import xbmcaddon

__addon__      = xbmcaddon.Addon()
__scriptname__ = __addon__.getAddonInfo('name')
__author__     = "prudu"
__GUI__        = "prudu"
__scriptid__   = __addon__.getAddonInfo('id')
__language__   = __addon__.getLocalizedString
__version__    = __addon__.getAddonInfo('version')
__cwd__        = __addon__.getAddonInfo('path')
__profile__    = xbmc.translatePath( __addon__.getAddonInfo('profile') ).decode("utf-8")
__resource__   = xbmc.translatePath( os.path.join( __cwd__, 'resources', 'lib' ) ).decode("utf-8")

sys.path.append (__resource__)

xbmc.log("##### [%s] - Version: %s" % (__scriptname__,__version__,),level=xbmc.LOGDEBUG )

if ( __name__ == "__main__" ):
    import gui
    if gui.SystemReady():
	ui = gui.GUI( "%s.xml" % __scriptid__.replace(".","-"), __cwd__, "Default")
    else:
	ui = gui.InstallerGUI( "install-rorw.xml", __cwd__, "Default")
    ui.doModal()
    del ui
    sys.modules.clear()
